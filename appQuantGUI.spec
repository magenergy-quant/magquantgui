# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['appQuantGUI.py'],
             pathex=['H:\\Projects\\magquantgui'],
             binaries=[],
             datas=[("Tools/Maps/ShapeFiles/Countries/UnitedStates/*.*", "Tools/Maps/ShapeFiles/Countries/UnitedStates"),
             ("Tools/Maps/ShapeFiles/Countries/Canada/*.*", "Tools/Maps/ShapeFiles/Countries/Canada"),
             ("Tools/Maps/ShapeFiles/Countries/Mexico/*.*", "Tools/Maps/ShapeFiles/Countries/Mexico"),
             ("Tools/Maps/ShapeFiles/ISO/*.*", "Tools/Maps/ShapeFiles/ISO"),
             ("Tools/Maps/ShapeFiles/ISO/ERCOT/*.*", "Tools/Maps/ShapeFiles/ISO/ERCOT"),
             ("Tools/Maps/ShapeFiles/ISO/ERCOT/*.*", "Tools/Maps/ShapeFiles/ISO/ERCOT"),
             ("Figures/Weather_Icones/*.*", "Figures/Weather_Icones")],
             hiddenimports=["pypyodbc", "tkinter", "pandas", "numpy", "datetime", "dateutil",
             "matplotlib", "matplotlib.pyplot", "matplotlib.backends.backend_tkagg",
             "PIL", "cartopy.crs", "cartopy.io.shapereader", "cartopy.feature"],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='appQuantGUI',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='appQuantGUI')
