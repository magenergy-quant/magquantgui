# -*- coding: utf-8 -*-
import tkinter as tk


class ScrollFrame(tk.Frame):
    def __init__(self, parent, min_width):
        super().__init__(parent)
        self.min_width = min_width
        
        self.canvas = tk.Canvas(self, borderwidth=0)
        self.viewPort = tk.Frame(self.canvas)
        self.vsb = tk.Scrollbar(self, orient="vertical", command=self.canvas.yview)
        self.hsb = tk.Scrollbar(self, orient="horizontal", command=self.canvas.xview)
        self.canvas.configure(yscrollcommand=self.vsb.set, xscrollcommand=self.hsb.set, bg="white")

        self.vsb.pack(side="right", fill="y")
        self.hsb.pack(side="bottom", fill="x")
        self.canvas.pack(side="left", fill="both", expand=True)
        self.canvas_window = self.canvas.create_window((0, 0), window=self.viewPort, anchor="nw",
                                                       tags="self.viewPort")#, width=parent.winfo_screenwidth())
        #self.viewPort.pack(side="left", fill="both", expand=True)
        self.viewPort.bind("<Configure>", lambda event: self.onFrameConfigure(event))
        self.canvas.bind("<Configure>", lambda event: self.onCanvasConfigure(event))
        self.height = self.canvas.winfo_reqheight()
        self.width = self.canvas.winfo_reqwidth()

        self.onFrameConfigure(None)

    def onFrameConfigure(self, event):
        '''Reset the scroll region to encompass the inner frame'''
        self.update_idletasks()
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))

    def onCanvasConfigure(self, event):
        '''Reset the canvas window to encompass inner frame when required'''
        canvas_width = event.width
        if canvas_width < self.min_width:
            canvas_width=self.min_width
        self.canvas.itemconfig(self.canvas_window, width=canvas_width)