# -*- coding: utf-8 -*-
import tkinter as tk
from tkinter import ttk
import Model.DataManipulation as dm
import pandas as pd
import datetime as dt
import dateutil as dtu
import matplotlib as mpl
import matplotlib.pyplot as plt
import Constants.constants as ct
from PIL import ImageTk, Image
import View.scrollframe as sf

mpl.use("TkAgg")

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk

class WeatherFrame():
    def __init__(self, master=None):
        self.master = master
        self.create_weather_frame()

    def create_weather_frame(self):
        self.frame_scrollable = sf.ScrollFrame(self.master, 1800)
        self.frame_scrollable.pack(side="left", fill="both", expand=True)
        self.frame_scrollable.viewPort.config(bg="white")

        drop_down_menu = self.init_drop_down_menu(self.frame_scrollable.viewPort)
        drop_down_menu.grid(row=1,
                            column=0,
                            pady=10,
                            padx=10,
                            sticky="w")
        
        update_button = tk.Button(self.frame_scrollable.viewPort, 
                                  text="CHOOSE",
                                  command=lambda: self.update_market(self.variable.get()),
                                  bg="white",
                                  height=1)
        update_button.grid(row=1,
                           column=0,
                           padx=150,
                           pady=10,
                           sticky="w")
        
    def update_market(self, market: tk.StringVar):
        if(market == "CHOOSE MARKET"):
            return
        
        self.choose_city(market)
   

    def choose_city(self, market: str):
        city_drop_cown = self.init_drop_down_city(self.frame_scrollable.viewPort, market)
        city_drop_cown.grid(row=1,
                            column=0,
                            pady=0,
                            padx=218,
                            sticky="w")
        
        choose_button = tk.Button(self.frame_scrollable.viewPort, 
                                  text="CHOOSE",
                                  command=lambda: self.update_frame(market, self.variable_city.get()),
                                  bg="white",
                                  height=1)
        choose_button.grid(row=1,
                           column=0,
                           padx=358,
                           pady=0,
                           sticky="w")
        
    def update_frame(self, market: str, city: str):
        frame_city_name = tk.Frame(self.frame_scrollable.viewPort)
        frame_city_name.grid(row=2, column=0, sticky="nw")
        frame_city_name.config(bg="white",relief=tk.SUNKEN)
        
        label_city_name = tk.Label(frame_city_name, text=city)
        label_city_name.grid(row=0, column=0, padx=10, sticky='nw')
        label_city_name.config(bg="white", font=("TkDefaultFont",18))
        
        self.frame_status = tk.Frame(self.frame_scrollable.viewPort)
        self.frame_status.grid(row=3, column=0, sticky="nwse")
        self.frame_status.columnconfigure((0,1,2,3), weight=1, uniform='quarter',
                                          minsize=350)#replace (0,1,2,3) with a constant expression
        self.frame_status.config(bg="white",relief=tk.SUNKEN)
        
        self.frame_cities = list()
        self.icon_paths = list()
        self.current_imgs = list()
        self.current_img_labels = list()
        
        for column_value in range(4):#replace with a constant value
            fm_current = tk.Frame(self.frame_status, bg = 'white',
                                  highlightbackground="black", 
                                  highlightthickness=1)
            
            fm_current.grid(row=0, column=column_value, sticky='n')
            fm_current.config(width= 300, height= 200)
            
            
            time_value = dt.datetime.now()+dt.timedelta(hours=column_value)
            time_str = "{} {} of {}, {}: {}".format(ct.weekday_names[time_value.weekday()],
                                                          time_value.day,
                                                          ct.month_names[time_value.month],
                                                          time_value.year,
                                                          time_value.strftime("%H:%m:%S"))
            
            current_tilte = tk.Label(fm_current, text=time_str)
            current_tilte.place(relx=0.01, rely=0.01, anchor='nw')
            current_tilte.config(bg="white", font=("TkDefaultFont",12))
            
            icon_path = 'Figures/Weather_Icones/Sunny.png'
            img = Image.open(icon_path)
            img = img.resize((100,100), Image.ANTIALIAS)
            current_img = ImageTk.PhotoImage(img)
            current_img_label = tk.Label(fm_current, image = current_img, bg="white")
            current_img_label.place(relx=0.01, rely=0.25, anchor='nw')
            
            self.icon_paths.append(icon_path)
            self.current_imgs.append(current_img)
            self.current_img_labels.append(current_img_label)
            
            label_texts = ["Temperature : 88 F", "Real Feel : 92 F",
                            "Precipitation : 60%", "Cloud Coverage : 60%"]
            
            for label_idx in range(len(label_texts)):
                label_temp = tk.Label(fm_current, text=label_texts[label_idx])
                label_temp.place(relx=0.41, rely=0.2 + 0.2*label_idx, anchor='nw')
                label_temp.config(bg="white", font=("TkDefaultFont",12))
            
            self.frame_cities.append(fm_current)
            
        self.frame_alert_graph = tk.Frame(self.frame_scrollable.viewPort)
        self.frame_alert_graph.grid(row=4, column=0, pady=10, sticky="nwse")
        self.frame_alert_graph.columnconfigure(0, weight=2, uniform='alert')#replace (0,1,2,3) with a constant expression
        self.frame_alert_graph.columnconfigure(1, weight=1, uniform='alert')
        self.frame_alert_graph.config(bg="white",relief=tk.SUNKEN)
        
        self.frame_alert = tk.Frame(self.frame_alert_graph,
                                    highlightbackground="black", 
                                    highlightthickness=1)
        self.frame_alert.grid(row=0, column=1, padx=40, pady=100, sticky="nwse")
        self.frame_alert.config(bg="white",relief=tk.SUNKEN)
        
        self.warning_icon_path = 'Figures/Weather_Icones/Warning.png'
        self.img_warning = Image.open(self.warning_icon_path)
        self.img_warning = self.img_warning.resize((100,100), Image.ANTIALIAS)
        self.current_warning_img = ImageTk.PhotoImage(self.img_warning)
        self.current_warning_img_label = tk.Label(self.frame_alert, image = self.current_warning_img,
                                                  bg="white")
        self.current_warning_img_label.grid(row=0, column=0, sticky="w")
        
        label_warning = tk.Label(self.frame_alert, text="ERCOT WARNINGS")
        label_warning.grid(row=0, column=0, padx=110, pady=50)
        label_warning.config(bg="white", font=("TkDefaultFont",20))
        
        message = """Dallas: High Temperature, possible thunderstorm and high cloud coverage could result in rapid diminution of load. To investigate.
        \nHouston: Nothing to report.
        \nSan Antonio: Nothing to report.
        \nAustin: Possible thunderstorms."""
        warning_message = tk.Message(self.frame_alert, text=message, bg="white", font=("TkDefaultFont",16))
        warning_message.config(anchor=tk.W, width=400)
        warning_message.grid(row=1, column=0, sticky="nw")
        
        self.graph_temp= 'Figures/FigureTemp.png'
        self.img_graph_temp = Image.open(self.graph_temp)
        self.img_graph_temp = self.img_graph_temp.resize((700,400), Image.ANTIALIAS)
        self.current_graph_temp_img = ImageTk.PhotoImage(self.img_graph_temp)
        self.current_graph_temp_img_label = tk.Label(self.frame_alert_graph, image = self.current_graph_temp_img,
                                                  bg="white")
        self.current_graph_temp_img_label.grid(row=0, column=0, padx=100, pady=150, sticky="w")
        
    def init_drop_down_menu(self, frame):
        self.variable = tk.StringVar(frame)
        self.variable.set(ct.market_names_drop_down[0])

        market_drop_down = tk.OptionMenu(frame, self.variable, *ct.market_names_drop_down)
        market_drop_down.config(width=15,
                                anchor="w",
                                bg="white",
                                height=1)
        market_drop_down["menu"].config(bg="white")
        market_drop_down["highlightthickness"] = 0

        return market_drop_down

    def init_drop_down_city(self, frame, market: str):
        self.variable_city = tk.StringVar(frame)
        self.variable_city.set(ct.city_names_drop_down[market][0])

        city_drop_down = tk.OptionMenu(frame, self.variable_city, *ct.city_names_drop_down[market])
        city_drop_down.config(width=15,
                                anchor="w",
                                bg="white",
                                height=1)
        city_drop_down["menu"].config(bg="white")
        city_drop_down["highlightthickness"] = 0
        
        return city_drop_down


    def destroy_weather_frame(self):
        self.frame_scrollable.pack_forget()
        self.frame_scrollable.destroy()
