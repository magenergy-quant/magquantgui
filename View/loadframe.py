# -*- coding: utf-8 -*-
import tkinter as tk
from tkinter import ttk
import Model.DataManipulation as dm
import pandas as pd
import datetime as dt
import dateutil as dtu
import matplotlib as mpl
import matplotlib.pyplot as plt
import Constants.constants as ct
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
import Tools.Maps.ercotmap as em
import View.scrollframe as sf

mpl.use("TkAgg")


class LoadFrame():
    def __init__(self, master=None):
        self.master = master
        self.create_load_frame()

    def create_load_frame(self):       
        self.frame_scrollable = sf.ScrollFrame(self.master, 1200)
        self.frame_scrollable.pack(side="left", fill="both", expand=True)

        self.frame_scrollable.viewPort.config(bg="white")
        self.frame_scrollable.viewPort.columnconfigure((0, 1), weight=1, uniform="half",
                                                       minsize=500)

        drop_down_menu = self.init_drop_down_menu(self.frame_scrollable.viewPort)
        drop_down_menu.grid(row=1,
                            column=0,
                            pady=10,
                            padx=10,
                            sticky="w")

        update_button = tk.Button(self.frame_scrollable.viewPort,
                                  text="UPDATE",
                                  command=lambda: self.update_market(self.variable.get()),
                                  bg="white")
        update_button.grid(row=1,
                           column=0,
                           padx=150,
                           pady=10,
                           sticky="w")

    def update_market(self, market: tk.StringVar):
        if(market == "CHOOSE MARKET"):
            return

        self.display_rt_actual_graphs(market)
    
    def display_rt_actual_graphs(self, market: str):
        # TO DO : Have Horizon Button (2 weeks, 1 month, 3 months, 6 months,
        # 1 year), for now, we use 3 month
        date_today = dt.datetime.now()
        date_beginning = (date_today - dtu.relativedelta.relativedelta(days=2)).date()
        
        print("Starting to obtained data")
        
        actual_data = dm.obtain_market_data(market, str(date_beginning))
        forecast_t2_data = dm.obtain_market_previous_forecasts(market, -2,
                                                                15.5, 16.5)
        forecast_t1_data = dm.obtain_market_previous_forecasts(market, -1,
                                                                7.5, 8.5)
        forecast_data = dm.obtain_market_forecasts(market)
        
        print("All data has been obtained")
        
        self.display_graph(actual_data["datemkt"], actual_data["actualload"],
                            forecast_t1_data["datemkt"], forecast_t1_data["forecastload_meteologica"],
                            forecast_t2_data["datemkt"], forecast_t2_data["forecastload_meteologica"],
                            forecast_data["datemkt"], forecast_data["forecastload_meteologica"],
                            3, 0, "Date (Jour Heure)", "Charge réelle (GW)", "Charge réelle")
        self.display_graph(actual_data["datemkt"], actual_data["actualwind"],
                            forecast_t1_data["datemkt"], forecast_t1_data["forecastwind_meteologica"],
                            forecast_t2_data["datemkt"], forecast_t2_data["forecastwind_meteologica"],
                            forecast_data["datemkt"], forecast_data["forecastwind_meteologica"],
                            5, 0, "Date (Jour Heure)", "Génération vent (GW)", "Génération par le vent")
        self.display_graph(actual_data["datemkt"], actual_data["actualresidualload"],
                            forecast_t1_data["datemkt"], forecast_t1_data["forecastresidualload_meteologica"],
                            forecast_t2_data["datemkt"], forecast_t2_data["forecastresidualload_meteologica"],
                            forecast_data["datemkt"], forecast_data["forecastresidualload_meteologica"],
                            7, 0, "Date (Jour Heure)", "Charge résiduelle (GW)", "Charge résiduelle")

        print("Finished plotting graphs")
        
        fig = em.create_ercot_map()
        canvas = FigureCanvasTkAgg(fig, master=self.frame_scrollable.viewPort)
        canvas.get_tk_widget().grid(row=3, column=1, rowspan=4, sticky="n")
        
        print("Finished creating ERCOT map")
        
        self.create_rt_dam_lmp(market)
        
    def create_rt_dam_lmp(self, market: str):
        data_lmps = dm.obtain_dam_rt_lmps(market)
        
        tree = ttk.Treeview(self.frame_scrollable.viewPort, columns = (1,2,3,4), height=9, show="headings")
        tree.grid(row=7, column=1, columnspan=2)
        
        column_names = ["Node", "Date and time", "DAM LMP", "RT LMP"]
        for i in range(len(column_names)):
            tree.heading(i+1, text=column_names[i], anchor="w")
            tree.column(i+1, width=150, anchor="w")
        
        for index, row_value in data_lmps.iterrows():
            tree.insert('', 'end', values=(row_value["node"], row_value["datemkt"], 
                                           row_value["dam_lmp"], row_value["rt_lmp"]))
        
    def display_graph(self, x_actual_load: pd.DataFrame, y_actual_load: pd.DataFrame,
                      x_forecast_t1: pd.DataFrame, y_forecast_t1: pd.DataFrame,
                      x_forecast_t2: pd.DataFrame, y_forecast_t2: pd.DataFrame,
                      x_forecast_h3: pd.DataFrame, y_forecast_h3: pd.DataFrame,
                      row_creation: int, column_creation: int, label_x : str, 
                      label_y: str, title: str):
        print("In display_graph")
        fig = plt.Figure(figsize=(8, 2.35))
        fig.set_dpi(99)
        ax = fig.add_subplot(111)
        
        print("Create figure")
        
        canvas = FigureCanvasTkAgg(fig, self.frame_scrollable.viewPort)
        canvas.get_tk_widget().grid(row=row_creation, column=column_creation)

        self.create_associate_toolbar(canvas, row_creation+1, column_creation)
        
        print("Created toolbar")
        
        ax.plot(x_actual_load, y_actual_load)
        ax.plot(x_forecast_t1, y_forecast_t1)
        ax.plot(x_forecast_t2, y_forecast_t2)
        ax.plot(x_forecast_h3, y_forecast_h3)
        ax.xaxis.set_major_formatter(mpl.dates.DateFormatter("%d %H:00"))
        fig.autofmt_xdate(bottom=0.32, rotation=30, ha='right')
        ax.grid(True, which="major")
        ax.tick_params(axis='x', labelsize=9)
        ax.tick_params(axis='y', labelsize=9)
        ax.set_xlabel(label_x, fontsize=10)
        ax.set_ylabel(label_y, fontsize=10)
        ax.set_title(title, fontsize=14, fontweight="bold")
        ax.legend(["Actual", "T-2", "T-1", "Most Recent"], fontsize=7, 
                   loc="upper left")

    def create_associate_toolbar(self, canvas: FigureCanvasTkAgg, 
                                 row_value: int, col_value):
        toolbar_frame = tk.Frame(master=self.frame_scrollable.viewPort)
        toolbar_frame.grid(row=row_value, column=col_value)
        toolbar_frame.config(bg="white")
        toolbar = NavigationToolbar2Tk(canvas,toolbar_frame)
        toolbar.config(background = "white", highlightthickness=0)
        toolbar._message_label.config(background="white")
        toolbar.update()
        
    def init_drop_down_menu(self, frame):
        self.variable = tk.StringVar(frame)
        self.variable.set(ct.market_names_drop_down[0])

        market_drop_down = tk.OptionMenu(frame, self.variable, *ct.market_names_drop_down)
        market_drop_down.config(width=15,
                                anchor="w",
                                bg="white")
        market_drop_down["menu"].config(bg="white")
        market_drop_down["highlightthickness"] = 0

        return market_drop_down

    def destroy_load_frame(self):
        self.frame_scrollable.pack_forget()
        self.frame_scrollable.destroy()
