# -*- coding: utf-8 -*-
import tkinter as tk
import View.loadframe as lf
import View.weatherframe as wf


class Window(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        
        self.weather_frame_displayed = False
        self.load_frame_displayed = False
        
        self.master = master
        self.init_window()

    def init_window(self):
        self.master.title("MAG Quant App")
        menu_bar = self.init_menu_bar()
        self.master.config(menu=menu_bar)

    def do_nothing(self):
        return

    def init_menu_bar(self):
        menu_bar = tk.Menu(self)
        rt_load = tk.Menu(menu_bar, tearoff=0)
        rt_temp = tk.Menu(menu_bar, tearoff=0)
        qt_analysis = tk.Menu(menu_bar, tearoff=0)

        load_checked = tk.IntVar(self)

        rt_load = tk.Menu(menu_bar, tearoff=0)
        rt_load.add_checkbutton(label="Global",
                                variable=load_checked,
                                onvalue=1,
                                offvalue=0,
                                command=lambda: self.manage_load_frame(load_checked.get()))
        rt_load.add_checkbutton(label="None",
                                command=self.do_nothing)
        menu_bar.add_cascade(label="RT Load", menu=rt_load)
        
        weather_checked = tk.IntVar(self)
        
        rt_temp = tk.Menu(menu_bar, tearoff=0)
        rt_temp.add_checkbutton(label="Global",
                                variable=weather_checked,
                                onvalue=1,
                                offvalue=0,
                                command=lambda: self.manage_weather_frame(weather_checked.get()))
        menu_bar.add_cascade(label="RT Temp", menu=rt_temp)
        qt_analysis = tk.Menu(menu_bar, tearoff=0)
        menu_bar.add_cascade(label="Quant Analysis", menu=qt_analysis)

        return menu_bar

    def manage_load_frame(self, status):
        if(self.weather_frame_displayed):
            self.weather_frame_displayed.destroy_load_frame()
            self.weather_frame_displayed = False
        if(status == 1):
            print("creating load frame")
            self.load_frame_displayed = True
            self.load_frame = lf.LoadFrame(self.master)
        else:
            print("destroying load frame")
            self.load_frame.destroy_load_frame()
            self.load_frame_displayed = False
        
    def manage_weather_frame(self, status):
        if(self.load_frame_displayed):
            self.load_frame.destroy_load_frame()
            self.load_frame_displayed = False
        if(status == 1):
            self.weather_frame = wf.WeatherFrame(self.master)
            self.weather_frame_displayed = True
        else:
            self.weather_frame.destroy_weather_frame()
            self.weather_frame_displayed = False
