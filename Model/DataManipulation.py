# -*- coding: utf-8 -*-
import pypyodbc
import Model.sqlqueries as sq
import pandas as pd
import numpy as np


def read_sql(SQLQuery):

    cnxn = pypyodbc.connect("Driver={SQL Server};"
                            "Server=MAGSQLSERVER;"
                            "Database=master;"
                            "Trusted_Connection=yes;")
    return pd.read_sql_query(SQLQuery, cnxn)

def obtain_dam_rt_lmps(market: str):
    return read_sql(sq.create_sql_dam_rt_lmps(market))

def obtain_market_data(market: str, date_begin: str):
    pd_data = pd.DataFrame()
    
    pd_actual_load = read_sql(sq.sql_query_load_data(market, date_begin))
    pd_actual_load["actualload"] = pd_actual_load["actualload"]/1000
    
    pd_actual_wind = read_sql(sq.sql_query_wind_data(market, date_begin))
    pd_actual_wind["actualwind"] = pd_actual_wind["actualwind"]/1000
    
    pd_data = pd_actual_load.merge(pd_actual_wind, left_on="datemkt",
                                   right_on="datemkt", how="outer")
    pd_data.insert(2,"actualresidualload", 
                   pd_data["actualload"].sub(pd_data["actualwind"],
                                             fill_value=np.nan))
    
    return pd_data

def obtain_market_previous_forecasts(market: str, horizon: int,
                                     time_begin_forecast: int, time_end_forecast: int):
    pd_data = pd.DataFrame()
    pd_forecast_load = read_sql(sq.sql_query_old_forecast_load(market, horizon,
                                                               time_begin_forecast,
                                                               time_end_forecast))
    pd_forecast_load["forecastload_meteologica"] = pd_forecast_load["forecastload_meteologica"]/1000
    
    pd_forecast_wind = read_sql(sq.sql_query_old_forecast_wind(market, horizon,
                                                               time_begin_forecast,
                                                               time_end_forecast))
    pd_forecast_wind["forecastwind_meteologica"] = pd_forecast_wind["forecastwind_meteologica"]/1000
    
    pd_data = pd_forecast_load.merge(pd_forecast_wind, left_on="datemkt",
                                     right_on="datemkt", how="outer")
    pd_data.insert(2, "forecastresidualload_meteologica", 
                   pd_data["forecastload_meteologica"].sub(pd_data["forecastwind_meteologica"],
                                             fill_value=np.nan))
    
    return pd_data

def obtain_market_forecasts(market: str):
    pd_data = pd.DataFrame()
    pd_forecast_load = read_sql(sq.sql_query_forecast_load(market))
    pd_forecast_load["forecastload_meteologica"] = pd_forecast_load["forecastload_meteologica"]/1000
    
    pd_forecast_wind = read_sql(sq.sql_query_forecast_wind(market))
    pd_forecast_wind["forecastwind_meteologica"] = pd_forecast_wind["forecastwind_meteologica"]/1000
    
    pd_data = pd_forecast_load.merge(pd_forecast_wind, left_on="datemkt",
                                     right_on="datemkt", how="outer")
    pd_data.insert(2, "forecastresidualload_meteologica", 
                   pd_data["forecastload_meteologica"].sub(pd_data["forecastwind_meteologica"],
                                             fill_value=np.nan))
    
    return pd_data