# -*- coding: utf-8 -*-

def sql_query_load_data(market: str, date_begin: str):
    if(market == "ERCOT"):
        sql_query = """SELECT MktData.HeDateCPT AS DateMkt,
        	timeT.EptTime AS PhotoDateMkt,
        	MktData.Value AS ActualLoad
        FROM YesEnergy.dbo.Timeseries_ByDataType_ERCOT AS MktData
        RIGHT JOIN(
        	SELECT MktData.HeDateCPT,
        		MAX(MktData.PhotoDateEPT) as MostRecentPhotoDate
        	FROM YesEnergy.dbo.Timeseries_ByDataType_ERCOT AS MktData
        	WHERE MktData.YesDataTypeId = 290 
        		AND MktData.YesObjectId = 10000712973
        	GROUP BY MktData.HeDateCPT
        ) newT
        ON MktData.HeDateCPT = newT.HeDateCPT AND MktData.PhotoDateEPT = newT.MostRecentPhotoDate
        INNER JOIN(
        	SELECT CAST(TimeZoneShift.FullUTC AS DATETIME) AS UtcTime,
        		CAST(TimeZoneShift.FULLEPT AS DATETIME) AS EptTime
        	FROM YesEnergy.dbo.MAG_REF_DateTimeByTimeZone AS TimeZoneShift
        ) timeT
        ON CAST(MktData.PhotoDateEPT AS DATE) = CAST(timeT.EptTime AS DATE) 
        	AND DATEPART(HOUR, MktData.PhotoDateEPT) = DATEPART(HOUR, timeT.EptTime)
        WHERE MktData.YesDataTypeId = 290 
        	AND MktData.YesObjectId = 10000712973
            AND MktData.HeDateCPT > '{}'
        ORDER BY MktData.HeDateCPT DESC""".format(date_begin)
        return sql_query
    
def sql_query_wind_data(market: str, date_begin: str):
    if(market == "ERCOT"):
        sql_query = """SELECT MktData.HeDateCPT AS DateMkt,
        	timeT.EptTime AS PhotoDateMkt,
        	MktData.Value AS ActualWind
        FROM YesEnergy.dbo.Timeseries_ByDataType_ERCOT AS MktData
        RIGHT JOIN(
        	SELECT MktData.HeDateCPT,
        		MAX(MktData.PhotoDateEPT) as MostRecentPhotoDate
        	FROM YesEnergy.dbo.Timeseries_ByDataType_ERCOT AS MktData
        	WHERE MktData.YesDataTypeId = 58 
        		AND MktData.YesObjectId = 10004189442
        	GROUP BY MktData.HeDateCPT
        ) newT
        ON MktData.HeDateCPT = newT.HeDateCPT AND MktData.PhotoDateEPT = newT.MostRecentPhotoDate
        INNER JOIN(
        	SELECT CAST(TimeZoneShift.FullUTC AS DATETIME) AS UtcTime,
        		CAST(TimeZoneShift.FULLEPT AS DATETIME) AS EptTime
        	FROM YesEnergy.dbo.MAG_REF_DateTimeByTimeZone AS TimeZoneShift
        ) timeT
        ON CAST(MktData.PhotoDateEPT AS DATE) = CAST(timeT.EptTime AS DATE) 
        	AND DATEPART(HOUR, MktData.PhotoDateEPT) = DATEPART(HOUR, timeT.EptTime)
        WHERE MktData.YesDataTypeId = 58 
        	AND MktData.YesObjectId = 10004189442
            AND MktData.HeDateCPT > '{}'
        ORDER BY MktData.HeDateCPT DESC""".format(date_begin)
        return sql_query

def sql_query_old_forecast_load(market: str, horizon: int, time_begin_forecast: int, 
                                time_end_forecast: int):
    if(market == "ERCOT"):
        sql_query = """SELECT MeteologicaData.HeDateMKT as DateMkt,
        	CAST(timeT.CptTime as DATETIME) as PhotoDateMkt,
        	MeteologicaData.perc50 as ForecastLoad_Meteologica
        FROM Meteologica.dbo.ERCOT_Power_Demand_forecasts_Meteologica as MeteologicaData
        RIGHT JOIN(
        	SELECT min(MeteologicaData.PhotoDateUTC) as ClosestPhotoDate
        	FROM Meteologica.dbo.ERCOT_Power_Demand_forecasts_Meteologica as MeteologicaData
        	WHERE MAG_REF_Zone__ID = 43
        		AND datepart(year, PhotoDateUTC) = datepart(year, dateadd(day,{}, GETDATE()))
        		AND datepart(month, PhotoDateUTC) = datepart(month, dateadd(day,{}, GETDATE()))
        		AND datepart(day, PhotoDateUTC) = datepart(day, dateadd(day,{}, GETDATE()))
        		AND datepart(hour, PhotoDateUTC) BETWEEN {} AND {}
        		AND MeteologicaData.HeDateMKT < dateadd(day,2, GETDATE())
        ) newT
        ON MeteologicaData.PhotoDateUTC = newT.ClosestPhotoDate
        INNER JOIN(
        	SELECT CAST(TimeZoneShift.FullUTC AS DATETIME) AS UtcTime,
        		CAST(TimeZoneShift.FullCPT AS DATETIME) AS CptTime
        	FROM YesEnergy.dbo.MAG_REF_DateTimeByTimeZone AS TimeZoneShift
        ) timeT
        ON CAST(MeteologicaData.PhotoDateUTC AS DATE) = CAST(timeT.CptTime AS DATE) 
        	AND DATEPART(HOUR, MeteologicaData.PhotoDateUTC) = DATEPART(HOUR, timeT.CptTime)
        WHERE MAG_REF_Zone__ID = 43
            AND MeteologicaData.HeDateMKT < dateadd(day,2, GETDATE())
        ORDER BY MeteologicaData.HeDateMKT""".format(horizon, horizon, horizon,
        time_begin_forecast, time_end_forecast)
        return sql_query
    
def sql_query_forecast_load(market: str):
    if(market == "ERCOT"):
        sql_query = """SELECT MeteologicaData.HeDateMKT as DateMkt,
        	CAST(timeT.CptTime as DATETIME) as PhotoDateMkt,
        	MeteologicaData.perc50 as ForecastLoad_Meteologica
        FROM Meteologica.dbo.ERCOT_Power_Demand_forecasts_Meteologica as MeteologicaData
        INNER JOIN(
        	SELECT MAX(MeteologicaData.PhotoDateUTC) as MostRecentPhoto
        	FROM Meteologica.dbo.ERCOT_Power_Demand_forecasts_Meteologica as MeteologicaData
            WHERE MeteologicaData.MAG_REF_Zone__ID = 43 
        ) newT
        ON MeteologicaData.PhotoDateUTC = newT.MostRecentPhoto
        INNER JOIN(
        	SELECT CAST(TimeZoneShift.FullUTC AS DATETIME) AS UtcTime,
        		CAST(TimeZoneShift.FullCPT AS DATETIME) AS CptTime
        	FROM YesEnergy.dbo.MAG_REF_DateTimeByTimeZone AS TimeZoneShift
        ) timeT
        ON CAST(MeteologicaData.PhotoDateUTC AS DATE) = CAST(timeT.CptTime AS DATE) 
        	AND DATEPART(HOUR, MeteologicaData.PhotoDateUTC) = DATEPART(HOUR, timeT.CptTime)
        WHERE MAG_REF_Zone__ID = 43 
        	AND MeteologicaData.HeDateMKT < DATEADD(DAY, 2, GETDATE())
        ORDER BY MeteologicaData.HeDateMKT"""
        return sql_query

def sql_query_old_forecast_wind(market: str, horizon: int, time_begin_forecast: int, 
                                time_end_forecast: int):
    if(market == "ERCOT"):
        sql_query = """SELECT MeteologicaData.HeDateMKT as DateMkt,
        	MeteologicaData.PhotoDateUTC,
            CAST(timeT.CptTime as DATETIME) as PhotoDateMkt,
            MeteologicaData.perc50 as ForecastWind_Meteologica
        FROM Meteologica.dbo.ERCOT_Wind_Power_Generation_forecasts_Meteologica as MeteologicaData
        RIGHT JOIN(
        	SELECT min(MeteologicaData.PhotoDateUTC) as ClosestPhotoDate
        	FROM Meteologica.dbo.ERCOT_Wind_Power_Generation_forecasts_Meteologica as MeteologicaData
        	WHERE MAG_REF_Zone__ID = 43
        		AND datepart(year, PhotoDateUTC) = datepart(year, dateadd(day,{}, GETDATE()))
        		AND datepart(month, PhotoDateUTC) = datepart(month, dateadd(day,{}, GETDATE()))
        		AND datepart(day, PhotoDateUTC) = datepart(day, dateadd(day,{}, GETDATE()))
        		AND datepart(hour, PhotoDateUTC) BETWEEN {} AND {}
        		AND MeteologicaData.HeDateMKT < dateadd(day,2, GETDATE())
        ) newT
        ON MeteologicaData.PhotoDateUTC = newT.ClosestPhotoDate
        INNER JOIN(
            SELECT CAST(TimeZoneShift.FullUTC AS DATETIME) AS UtcTime,
                CAST(TimeZoneShift.FullCPT AS DATETIME) AS CptTime
            FROM YesEnergy.dbo.MAG_REF_DateTimeByTimeZone AS TimeZoneShift
        ) timeT
        ON CAST(MeteologicaData.PhotoDateUTC AS DATE) = CAST(timeT.CptTime AS DATE) 
            AND DATEPART(HOUR, MeteologicaData.PhotoDateUTC) = DATEPART(HOUR, timeT.CptTime)
        WHERE MAG_REF_Zone__ID = 43
            AND MeteologicaData.HeDateMKT < dateadd(day,2, GETDATE())
        ORDER BY MeteologicaData.HeDateMKT""".format(horizon, horizon, horizon, time_begin_forecast,
            time_end_forecast)
        return sql_query
    
def sql_query_forecast_wind(market: str):
    if(market == "ERCOT"):
        sql_query = """SELECT MeteologicaData.HeDateMKT as DateMkt,
        	CAST(timeT.CptTime as DATETIME) as PhotoDateMkt,
        	MeteologicaData.perc50 as ForecastWind_Meteologica
        FROM Meteologica.dbo.ERCOT_Wind_Power_Generation_forecasts_Meteologica as MeteologicaData
        INNER JOIN(
        	SELECT MAX(MeteologicaData.PhotoDateUTC) as MostRecentPhoto
        	FROM Meteologica.dbo.ERCOT_Wind_Power_Generation_forecasts_Meteologica as MeteologicaData
            WHERE MeteologicaData.MAG_REF_Zone__ID = 43 
        ) newT
        ON MeteologicaData.PhotoDateUTC = newT.MostRecentPhoto
        INNER JOIN(
        	SELECT CAST(TimeZoneShift.FullUTC AS DATETIME) AS UtcTime,
        		CAST(TimeZoneShift.FullCPT AS DATETIME) AS CptTime
        	FROM YesEnergy.dbo.MAG_REF_DateTimeByTimeZone AS TimeZoneShift
        ) timeT
        ON CAST(MeteologicaData.PhotoDateUTC AS DATE) = CAST(timeT.CptTime AS DATE) 
        	AND DATEPART(HOUR, MeteologicaData.PhotoDateUTC) = DATEPART(HOUR, timeT.CptTime)
        WHERE MAG_REF_Zone__ID = 43 
        	AND MeteologicaData.HeDateMKT < DATEADD(DAY, 2, GETDATE())
        ORDER BY MeteologicaData.HeDateMKT"""
        return sql_query
    
def create_sql_dam_rt_lmps(market: str):
    if(market == "ERCOT"):
        sql_query = """SELECT CAST(timeT.FullCPT as DATETIME) as DateMkt,
        	ref.Node,
        	val.DAM_LMP,
        	val.RT_LMP
        FROM Alexandria.dbo.ERCOTNodesHourly val
        INNER JOIN MAGVirtual.dbo.ListHubERCOT ref
        ON val.[ID] = ref.[ID]
        RIGHT JOIN(
        	SELECT ref.Node,
        		max(val.dateCST) as EarliestLMPDate
        	FROM Alexandria.dbo.ERCOTNodesHourly val
        	INNER JOIN MAGVirtual.dbo.ListHubERCOT ref
        	ON val.[ID] = ref.[ID]
        	WHERE val.RT_LMP is not null
        		AND (ref.Node = 'HB_west' 
        			OR ref.Node = 'HB_south'
        			OR ref.Node = 'HB_north'
        			OR ref.Node = 'HB_Houston'
        			OR ref.Node = 'DC_E'
        			OR ref.Node = 'DC_L'
        			OR ref.Node = 'DC_N'
        			OR ref.Node = 'DC_R'
        			OR ref.Node = 'DC_S')
        	GROUP BY ref.Node
        ) newT
        ON ref.Node = newT.Node AND val.dateCST = newT.EarliestLMPDate
        INNER JOIN(
        SELECT FullEPT,
        	FullCPT
        FROM YesEnergy.dbo.MAG_REF_DateTimeByTimeZone
        )timeT
        ON val.dateCST = CAST(timeT.FullEPT AS DATETIME)"""
        return sql_query