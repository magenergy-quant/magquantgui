market_names_drop_down = ["CHOOSE MARKET",
                          "AESO",
                          "CAISO",
                          "ERCOT",
                          "IESO",
                          "ISONE",
                          "MISO",
                          "NYISO",
                          "PJM",
                          "SPP"]

city_names_drop_down = {
    "ERCOT": ["CHOOSE CITY",
              "Dallas",
              "Houston",
              "San Antonio",
              "Austin"]
    }

weekday_names = {
    0:"Monday",
    1:"Tuesday",
    2:"Wednesday",
    3:"Thursday",
    4:"Friday",
    5:"Saturday",
    6:"Sunday",
    }

month_names = {
    1:"January",
    2:"February",
    3:"March",
    4:"April",
    5:"May",
    6:"June",
    7:"July",
    8:"August",
    9:"September",
    10:"October",
    11:"November",
    12:"December",
    }