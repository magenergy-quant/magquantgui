# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 12:57:02 2020

@author: mmasse
"""


import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.io.shapereader import Reader
from cartopy.io.shapereader import Record
from cartopy.feature import ShapelyFeature
import cartopy.io.shapereader as shpreader

# Pour prices, respecter l'order:
    # ercotn 
    # ercote 
    # lam345
    # scse
    # blkw 
    # mcwest
    # NORTH HUB
    # SOUTH HUB
    

def create_map():

    # Shape file ISO
    # Source:
    # https://hifld-geoplatform.opendata.arcgis.com/datasets/geoplatform::independent-system-operators?edit=false&filterByExtent=true&fullScreen=false&geometry=-169.256%2C25.582%2C-23.093%2C49.568&orderBy=&orderByAsc=true&page=1&where=
    fname = r'iso/Independent_System_Operators.shp'
    
    # List iso disponible
    sf = Reader(fname)
    shape_records = sf.records()
    
    iso = []
    for s in shape_records:
        iso.append(s.attributes['NAME'])
        
    # Choix ISO en question  
    shape_records = sf.records()
    shape = []
    for s in shape_records:
        if s.attributes['NAME'] == iso[1]: # SPP
            shape.append(s)
            
    # Shape File state
    shapename = 'admin_1_states_provinces_lakes_shp'
    states_shp = shpreader.natural_earth(resolution='110m',category='cultural', name=shapename)
       
    
       
    # Plot ISO
    fig = plt.figure(figsize=(7, 7))
    ax = fig,add_subplot(111, projection=ccrs.PlateCarree())      
    ax.add_geometries(shape[0].geometry,ccrs.Mercator(), edgecolor='black', alpha = 0.7)
    ax.add_geometries(shpreader.Reader(states_shp).geometries(),ccrs.PlateCarree(), edgecolor='black', alpha = 0.5, Facecolor='white')
    ax.set_extent([-110, -90, 30, 50])
    # ax.set_extent([-135, -130, 32, 60])
    
    # Add hubs location
    ercotn = [-98.99,33.94]
    ercote = [-95.3857, 32.1011]
    lam345 = [-102.4217, 38.5]
    scse = [-103.5, 41.5085]
    blkw = [-105, 33.72434]
    mcwest = [-105, 46.37725]
    northhub = [-96.621022, 40.797177]
    southhub = [-98.101675, 35.317366]
    
    # # Create color from price
    # cmap = plt.cm.get_cmap('jet')
    
    # colors = []
    # for price in prices:
        
    #     if price < 0:
    #         colors.append(cmap(0))
    #     elif  0 < price <= 40:
    #         colors.append(cmap(0.2))
    #     elif 40 < price <= 80:
    #         colors.append(cmap(0.4))
    #     elif 80 < price <= 120:
    #         colors.append(cmap(0.6))
    #     elif 120 < price <= 300:
    #         colors.append(cmap(0.8))
    #     elif 300 < price :
    #         colors.append(cmap(1))
            
        
    
    ax.plot(ercotn[0], ercotn[1], 'D', linewidth=2, markersize=12, transform = ccrs.Geodetic(), color='red')
    ax.plot(ercote[0], ercote[1], 'D', linewidth=2, markersize=12, transform = ccrs.Geodetic(), color='red')
    ax.plot(lam345[0], lam345[1], 'D', linewidth=2, markersize=12, transform = ccrs.Geodetic(), color='red')
    ax.plot(scse[0], scse[1], 'D', linewidth=2, markersize=12, transform = ccrs.Geodetic(), color='red')
    ax.plot(blkw[0], blkw[1], 'D', linewidth=2,  markersize=12, transform = ccrs.Geodetic(), color='red')
    ax.plot(mcwest[0], mcwest[1], 'D', linewidth=2,  markersize=12, transform = ccrs.Geodetic(), color='red')
    ax.plot(northhub[0], northhub[1], 's', linewidth=2,  markersize=12, transform = ccrs.Geodetic(), color='green')
    ax.plot(southhub[0], southhub[1], 's', linewidth=2,  markersize=12, transform = ccrs.Geodetic(), color='green')
    
    # add text
    ax.text (ercotn[0], ercotn[1]-1, infos[0], horizontalalignment='right',transform=ccrs.Geodetic() )
    ax.text (ercote[0]-0.7, ercote[1]-0.3, infos[1], horizontalalignment='right',transform=ccrs.Geodetic() )
    ax.text (lam345[0]-0.5, lam345[1], infos[2], horizontalalignment='right',transform=ccrs.Geodetic() )
    ax.text (scse[0]-0.6, scse[1]-0.3, infos[3], horizontalalignment='right',transform=ccrs.Geodetic() )
    ax.text (blkw[0]-0.6, blkw[1]-0.3, infos[4], horizontalalignment='right',transform=ccrs.Geodetic() )
    ax.text (mcwest[0]-0.5, mcwest[1]-0.2, infos[5], horizontalalignment='right',transform=ccrs.Geodetic() )
    ax.text (northhub[0]-0.5, northhub[1]-0.2, infos[6], horizontalalignment='right',transform=ccrs.Geodetic() )
    ax.text (southhub[0]+2.2, southhub[1]-0.2, infos[7], horizontalalignment='right',transform=ccrs.Geodetic() )
        
    # plt.show()
    return fig



