# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 10:54:57 2020

@author: jpilon
"""
import matplotlib
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.io.shapereader import Reader
from cartopy.io.shapereader import Record
from cartopy.feature import ShapelyFeature
import cartopy.io.shapereader as shpreader

matplotlib.use("TkAgg")
# Plot ISO
def create_ercot_map():
    print("create_ercot_map")
    fig = plt.figure(figsize=(6,6))
    fig.set_dpi(99)
    fig.subplots_adjust(left=0.03, bottom=0.07, right=0.98, top=0.97, wspace=0, hspace=0)
    ax = fig.add_subplot(111, projection=ccrs.PlateCarree())      
    # ax.add_geometries(shape.geometry,ccrs.Mercator(), edgecolor='black', alpha = 0.7)
    ax.add_geometries(shpreader.Reader(r'Tools/Maps/ShapeFiles/Countries/UnitedStates/tl_2017_us_state.shp').geometries(),ccrs.PlateCarree(), edgecolor='black', alpha = 1, Facecolor='white') #USA
    ax.add_geometries(shpreader.Reader(r'Tools/Maps/ShapeFiles/Countries/Mexico/mexstates.shp').geometries(),ccrs.PlateCarree(), edgecolor='black', alpha = 1, Facecolor='white') #MEXICO
    #ax.add_geometries(shpreader.Reader(r'iso/gpr_000b11a_e.shp').geometries(),ccrs.PlateCarree(), edgecolor='black', alpha = 1, Facecolor='white') #CANADA
    ax.add_geometries(Reader(r'Tools/Maps/ShapeFiles/ISO/ERCOT/ercot_boundary-polygon.shp').geometries(), ccrs.PlateCarree(), edgecolor='blue', alpha = 0.5)
    #ax.set_extent([-110, -90, 30, 50]) #SPP
    ax.set_extent([-108, -92, 25, 38]) #ERCOT
    #ax.set_extent([-135, -50, 0, 60])
    
    # Add hubs location
    ercot_dc_tie_n = [-98.99,33.94]
    ercot_dc_tie_e = [-95.097656, 33.449777]
    ercot_dc_tie_s = [-100.502930, 28.680950]
    ercot_dc_tie_l = [-99.470215, 27.547242]
    ercot_dc_tie_r = [-98.547363, 26.234302]
    
    ercot_hub_n = [-97.119141, 32.026706]
    ercot_hub_s = [-98.613281, 29.649869]
    ercot_hub_e = [-100.854492, 31.952162]
    ercot_hub_h = [-95.449219, 29.535230]
    
    legend_d = [-94, 27]
    legend_s = [-94, 26]
    
    print("middle create_ercot_map")
    
    ax.plot(ercot_dc_tie_n[0], ercot_dc_tie_n[1], 'D', linewidth=2, markersize=12, transform = ccrs.Geodetic(), color='red')
    ax.plot(ercot_dc_tie_e[0], ercot_dc_tie_e[1], 'D', linewidth=2, markersize=12, transform = ccrs.Geodetic(), color='red')
    ax.plot(ercot_dc_tie_s[0], ercot_dc_tie_s[1], 'D', linewidth=2, markersize=12, transform = ccrs.Geodetic(), color='red')
    ax.plot(ercot_dc_tie_l[0], ercot_dc_tie_l[1], 'D', linewidth=2, markersize=12, transform = ccrs.Geodetic(), color='red')
    ax.plot(ercot_dc_tie_r[0], ercot_dc_tie_r[1], 'D', linewidth=2, markersize=12, transform = ccrs.Geodetic(), color='red')
    
    ax.plot(ercot_hub_n[0], ercot_hub_n[1], 's', linewidth=2, markersize=12, transform = ccrs.Geodetic(), color='green')
    ax.plot(ercot_hub_s[0], ercot_hub_s[1], 's', linewidth=2, markersize=12, transform = ccrs.Geodetic(), color='green')
    ax.plot(ercot_hub_e[0], ercot_hub_e[1], 's', linewidth=2, markersize=12, transform = ccrs.Geodetic(), color='green')
    ax.plot(ercot_hub_h[0], ercot_hub_h[1], 's', linewidth=2, markersize=12, transform = ccrs.Geodetic(), color='green')
    
    ax.plot(legend_d[0], legend_d[1], 's', linewidth=2, markersize=12, transform = ccrs.Geodetic(), color='green')
    ax.plot(legend_s[0], legend_s[1], 'D', linewidth=2, markersize=12, transform = ccrs.Geodetic(), color='red')
    
    # add text
    ax.text (ercot_dc_tie_n[0]-0.25, ercot_dc_tie_n[1]+0.5, "North", horizontalalignment='left',transform=ccrs.Geodetic() )
    ax.text (ercot_dc_tie_e[0]-0.33, ercot_dc_tie_e[1]+0.60, "East", horizontalalignment='left',transform=ccrs.Geodetic() )
    ax.text (ercot_dc_tie_s[0]+0.5, ercot_dc_tie_s[1]-0.12, "Eagle Pass", horizontalalignment='left',transform=ccrs.Geodetic() )
    ax.text (ercot_dc_tie_l[0]+0.5, ercot_dc_tie_l[1]-0.12, "Laredo", horizontalalignment='left',transform=ccrs.Geodetic() )
    ax.text (ercot_dc_tie_r[0]+0.5, ercot_dc_tie_r[1]-0.12, "Railroad", horizontalalignment='left',transform=ccrs.Geodetic() )
    
    ax.text (ercot_hub_n[0]-0.25, ercot_hub_n[1]+0.5, "North", horizontalalignment='left',transform=ccrs.Geodetic() )
    ax.text (ercot_hub_s[0]-0.25, ercot_hub_s[1]+0.5, "South", horizontalalignment='left',transform=ccrs.Geodetic() )
    ax.text (ercot_hub_e[0]-0.25, ercot_hub_e[1]+0.5, "East", horizontalalignment='left',transform=ccrs.Geodetic() )
    ax.text (ercot_hub_h[0]-0.25, ercot_hub_h[1]+0.5, "Houston", horizontalalignment='left',transform=ccrs.Geodetic() )
    
    ax.text (legend_d[0]+0.5, legend_d[1]-0.17, "Hub", horizontalalignment='left',transform=ccrs.Geodetic() )
    ax.text (legend_s[0]+0.5, legend_s[1]-0.17, "DC Tie", horizontalalignment='left',transform=ccrs.Geodetic() )
    
    print("end")
    
    return fig